package com.dmd.timer.utils;

public final class Converter {

    private Converter() {
    }

    public static float inMetresPerSec(float kmPerHour) {
        return (kmPerHour * 1000 / 3600);
    }

    public static int inKmPerHours(float metresPerSec) {
        return Math.round((metresPerSec * 3600 / 1000));
    }

    public static String[] msToMinAndSecStringArray(long ms) {
        int seconds = (int) (ms / 1000) % 60;
        int minutes = (int) ((ms / (1000 * 60)) % 60);
        String[] minutesAndSeconds = new String[2];
        minutesAndSeconds[0] = numberToString(minutes);
        minutesAndSeconds[1] = numberToString(seconds);
        return minutesAndSeconds;
    }

    private static String numberToString(int number) {
        if (number < 10) {
            return String.format("0%d", number);
        } else {
            return String.format("%d", number);
        }
    }

}
