package com.dmd.timer.utils;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.TextView;

import java.util.Hashtable;

public final class FontsUtils {

    private static final String TAG = FontsUtils.class.getName();
    private static final String FONTS_PATH = "fonts/";
    private static final String NEUROPOL = FONTS_PATH + "neuropol.TTF";
    private static final String GOTHIC = FONTS_PATH + "gothic.TTF";
    private static final String GOTHIC_BOLD = FONTS_PATH + "gothic_bold.ttf";

    private static Hashtable<String, Typeface> typefaceCache = new Hashtable<>();

    private FontsUtils() {
    }

    public static final void setNeuropol(AssetManager assetManager, TextView textView) {
        textView.setTypeface(get(NEUROPOL, assetManager));
    }

    public static final void setGothic(AssetManager assetManager, TextView textView) {
        textView.setTypeface(get(GOTHIC, assetManager));
    }

    public static final void setGothicBold(AssetManager assetManager, TextView textView) {
        textView.setTypeface(get(GOTHIC_BOLD, assetManager));
    }

    private static Typeface get(String typefaceName, AssetManager assetManager) {
        Typeface typeface = typefaceCache.get(typefaceName);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(assetManager, typefaceName);
            } catch (Exception e) {
                Log.e(TAG, "Failed to create asset: " + e);
                return null;
            }
            typefaceCache.put(typefaceName, typeface);
        }
        return typeface;
    }

}
