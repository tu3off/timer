package com.dmd.timer.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.NotificationCompat;

import com.dmd.timer.R;
import com.dmd.timer.ui.activity.home.HomeActivity;
import com.dmd.timer.ui.activity.settings.NotificationTextActivity;
import com.dmd.timer.ui.activity.settings.NotificationVoiceRecordActivity;

public final class NotificationHelper {

    private static final int NOTIFICATION_ID = 2;
    private static final int REQUEST_CODE = 1;

    private NotificationHelper() {
    }

    public static void buildAndShowTextNotification(String contentText, String path, Context context) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        Intent notificationIntent = new Intent(context, NotificationTextActivity.class);
        if (contentText.length() > 20) {
            contentText = contentText.substring(0, 19) + "...";
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(context, REQUEST_CODE,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(contentText)
                .setContentTitle("Timer text")
                .setContentIntent(pendingIntent)
                .setSound(Uri.parse(path));
        showNotification(builder.build(), context);
    }

    public static void buildAndShowVoiceNotification(String record, String path, Context context) {
        Intent notificationIntent = new Intent(context, NotificationVoiceRecordActivity.class);
        notificationIntent.putExtra(NotificationVoiceRecordActivity.PATH_BUNDLE_KEY, record);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, REQUEST_CODE,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Timer voice")
                .setContentIntent(pendingIntent)
                .setSound(Uri.parse(path));
        showNotification(builder.build(), context);
    }

    private static void showNotification(Notification notification, Context context) {
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    public static Notification buildForegroundNotification(Context context) {
        Intent notificationIntent = new Intent(context, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, REQUEST_CODE,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(pendingIntent);
        builder.setContentTitle("Timer"); // Todo: replace hardcode
        builder.setContentText("foreground"); // Todo: replace hardcode
        builder.setSmallIcon(R.mipmap.ic_launcher); // Todo: replace hardcode
        return builder.build();
    }

}