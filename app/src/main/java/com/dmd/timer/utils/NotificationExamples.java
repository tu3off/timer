package com.dmd.timer.utils;

public final class NotificationExamples {

    public static final String TEXT_NOTIFICATION_EXAMPLE_1 = "Notification example 1";
    public static final String TEXT_NOTIFICATION_EXAMPLE_2 = "Notification example 2";
    public static final String TEXT_NOTIFICATION_EXAMPLE_3 = "Notification example 3";
    public static final String TEXT_NOTIFICATION_EXAMPLE_4 = "Notification example 4";

    private NotificationExamples() {
    }

}
