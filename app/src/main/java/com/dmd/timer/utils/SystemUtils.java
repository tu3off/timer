package com.dmd.timer.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.File;

public final class SystemUtils {

    private static final String ANDROID_RESOURCE_SCHEMA = "android.resource://";
    private static final String TEMP_FILE_NAME = "temp.mp3";

    private SystemUtils() {
    }

    public static String rawResourceToUriString(Context context, int resId) {
        return ANDROID_RESOURCE_SCHEMA + context.getPackageName() + File.separator + resId;
    }

    public static String pathToTempFile(Context context) {
        return context.getCacheDir().getAbsolutePath() + File.separator + TEMP_FILE_NAME;
    }

    public static void hideKeyboard(Context context, View view) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE))
                .toggleSoftInput(InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
}
