package com.dmd.timer.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import com.dmd.timer.service.TrackerService;
import com.dmd.timer.ui.activity.home.ContactUsActivity;
import com.dmd.timer.ui.activity.home.HomeActivity;
import com.dmd.timer.ui.activity.settings.NotificationSettingsActivity;
import com.dmd.timer.ui.activity.settings.NotificationVoiceRecordActivity;
import com.dmd.timer.ui.activity.useragreement.UserAgreementActivity;

public final class Launcher {

    private Launcher() {
    }

    public static final class service {

        private service() {
        }

        public static final void startTracker(Context context) {
            context.startService(new Intent(context, TrackerService.class));
        }

        public static final void stopTracker(Context context) {
            context.stopService(new Intent(context, TrackerService.class));
        }

    }

    public static final class activity {

        public static final int LOCATION_SOURCE_SETTINGS_REQUEST_CODE = 1;

        private activity() {
        }

        public static void userAgreement(Activity activity) {
            Intent intent = new Intent(activity, UserAgreementActivity.class);
            activity.startActivity(intent);
        }

        public static void home(Activity activity) {
            Intent intent = new Intent(activity, HomeActivity.class);
            activity.startActivity(intent);
        }

        public static void contactUs(Activity activity) {
            Intent intent = new Intent(activity, ContactUsActivity.class);
            activity.startActivity(intent);
        }

        public static void notificationVoiceRecord(Activity activity) {
            Intent intent = new Intent(activity, NotificationVoiceRecordActivity.class);
            activity.startActivity(intent);
        }

        public static void notificationSettings(Activity activity) {
            Intent intent = new Intent(activity, NotificationSettingsActivity.class);
            activity.startActivity(intent);
        }

        public static void actionLocationSourceSettings(Activity activity) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            activity.startActivityForResult(intent, LOCATION_SOURCE_SETTINGS_REQUEST_CODE);
        }

    }

}
