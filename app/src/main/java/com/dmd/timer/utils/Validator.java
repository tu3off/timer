package com.dmd.timer.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Validator {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
            "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private Validator() {
    }

    public static boolean isValidEmail(String inputtedEmail) {
        Matcher matcher = Pattern.compile(EMAIL_PATTERN).matcher(inputtedEmail);
        return matcher.matches();
    }

}
