package com.dmd.timer.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.dmd.timer.core.TheApplication;
import com.dmd.timer.core.settings.SharedHelper;
import com.dmd.timer.service.observer.TimerObservable;
import com.dmd.timer.service.observer.TimerObserver;
import com.dmd.timer.utils.NotificationHelper;
import com.dmd.timer.utils.SystemUtils;

public class TrackerService extends Service {

    private static final int COUNT_DOWN_TIMER_INTERVAL = 250;
    private static final int POST_DELAY_MS = 2 * 1_000 * 60;
    private static final int FOREGROUND_NOTIFICATION_ID = 1;
    private static final float LOCATION_DISTANCE = 1f;
    private static final int LOCATION_INTERVAL = 1_000;
    private static final String TAG = TrackerService.class.getSimpleName();

    private final IBinder binder = new TrackerBinder();
    private boolean trackerStarted;
    private CountDown countDown;
    private GPSLocationListener locationListeners;
    private Handler handler;
    private LocationManager locationManager;
    private Runnable postDelayStartTimer;
    private SharedHelper sharedHelper;
    private TimerObservable timerObservable;
    private TheApplication theApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        postDelayStartTimer = new PostDelayedStartTimer();
        timerObservable = new TimerObservable();
        theApplication = (TheApplication) getApplicationContext();
        sharedHelper = theApplication.getSharedHelper();
        locationManager = theApplication.getLocationManager();
        updateTime();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForegroundTracker();
        locationManager = null;
    }

    public void startForegroundTracker() {
        timerObservable.setTime(sharedHelper.getTime());
        locationListeners = new GPSLocationListener(sharedHelper.getSpeed(),
                LocationManager.GPS_PROVIDER);
        try {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    locationListeners);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
        trackerStarted = true;
        startForeground();
    }

    public void stopForegroundTracker() {
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(locationListeners);
            } catch (Exception ex) {
                Log.i(TAG, "fail to remove location listeners, ignore", ex);
            }
        }
        stopCountDownTimer();
        trackerStarted = false;
        stopForeground();
    }

    public void addObserver(TimerObserver observer) {
        timerObservable.addObserver(observer);
    }

    public void removeObserver(TimerObserver observer) {
        timerObservable.removeObserver(observer);
    }

    public long getTime() {
        return timerObservable.getTime();
    }

    public void updateTime() {
        timerObservable.setTime(sharedHelper.getTime());
    }

    public boolean isTimerFinished() {
        return timerObservable.isTimerFinished();
    }

    public boolean isTrackerStarted() {
        return trackerStarted;
    }

    private void startForeground() {
        startForeground(FOREGROUND_NOTIFICATION_ID,
                NotificationHelper.buildForegroundNotification(this));
    }

    private void stopForeground() {
        stopForeground(true);
    }

    private void startCountDownTimer() {
        handler.postDelayed(postDelayStartTimer, POST_DELAY_MS);
    }

    private void stopCountDownTimer() {
        handler.removeCallbacks(postDelayStartTimer);
        if (countDown != null) {
            countDown.cancel();
        }
    }

    private void throwNotification() {
        if (sharedHelper.getTypeNotification() == SharedHelper.TYPE_TEXT) {
            NotificationHelper.buildAndShowTextNotification(sharedHelper.getTextNotification(),
                    sharedHelper.getSoundPath(),
                    getApplicationContext());
        } else {
            NotificationHelper.buildAndShowVoiceNotification(
                    SystemUtils.pathToTempFile(this),
                    sharedHelper.getSoundPath(),
                    getApplicationContext());
        }
    }

    public class TrackerBinder extends Binder {

        public TrackerService getService() {
            return TrackerService.this;
        }

    }

    private class CountDown extends CountDownTimer {

        public CountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timerObservable.setTime(millisUntilFinished);
        }

        @Override
        public void onFinish() {
            throwNotification();
            timerObservable.setTimerFinished(true);
            updateTime();
            stopForegroundTracker();
        }
    }

    private class GPSLocationListener implements LocationListener {

        private boolean counting;
        private Location location;
        private float speedLimit;

        public GPSLocationListener(float speedLimit, String provider) {
            this.speedLimit = speedLimit;
            location = new Location(provider);
            counting = true;
        }

        @Override
        public void onLocationChanged(Location location) {
            this.location.set(location);
            if (location.getSpeed() < speedLimit && !counting) {
                startCountDownTimer();
                counting = true;
            } else if (location.getSpeed() > speedLimit && counting) {
                stopCountDownTimer();
                counting = false;
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }

    }

    private class PostDelayedStartTimer implements Runnable {

        @Override
        public void run() {
            countDown = new CountDown(sharedHelper.getTime(), COUNT_DOWN_TIMER_INTERVAL);
            countDown.start();
        }
    }

}