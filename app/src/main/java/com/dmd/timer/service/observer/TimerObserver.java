package com.dmd.timer.service.observer;

public interface TimerObserver extends Observer {
    void updateTimer(long time);
    void timerFinished(boolean timerFinished);
}
