package com.dmd.timer.service.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TimerObservable implements Observable<TimerObserver> {

    private List<TimerObserver> timerObservers;
    private long time;
    private boolean timerFinished;

    public TimerObservable() {
        timerObservers = new ArrayList<>();
    }

    public long getTime() {
        return time;
    }

    public boolean isTimerFinished() { return timerFinished; }

    public void setTime(long time) {
        this.time = time;
        notifyObservers();
    }

    public void setTimerFinished(boolean timerFinished) {
        this.timerFinished = timerFinished;
        notifyTimerFinishedObservers();
    }

    public void notifyTimerFinishedObservers() {
        Iterator<TimerObserver> iterator = timerObservers.iterator();
        while (iterator.hasNext()) {
            TimerObserver observer = iterator.next();
            observer.timerFinished(timerFinished);
        }
    }

    @Override
    public void addObserver(TimerObserver observer) {
        if (!timerObservers.contains(observer)) {
            timerObservers.add(observer);
        }
    }

    @Override
    public void removeObserver(TimerObserver observer) {
        if (timerObservers.contains(observer)) {
            timerObservers.remove(observer);
        }
    }

    @Override
    public void notifyObservers() {
        Iterator<TimerObserver> iterator = timerObservers.iterator();
        while (iterator.hasNext()) {
            TimerObserver observer = iterator.next();
            observer.updateTimer(time);
        }
    }
}