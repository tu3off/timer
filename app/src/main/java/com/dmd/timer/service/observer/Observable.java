package com.dmd.timer.service.observer;

public interface Observable<T extends Observer> {
    void addObserver(T observer);

    void removeObserver(T observer);

    void notifyObservers();
}
