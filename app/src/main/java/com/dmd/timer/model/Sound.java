package com.dmd.timer.model;

public class Sound {

    private String title;
    private String path;

    public Sound(String title, String path) {
        this.title = title;
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public String getPath() {
        return path;
    }
}
