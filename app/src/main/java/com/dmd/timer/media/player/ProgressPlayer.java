package com.dmd.timer.media.player;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;

public class ProgressPlayer extends Player {

    private static final int CHECK_POSITION_THREAD_DELAY = 250;
    private OnPlayerListener playerListener;
    private Runnable checkPositionStateThread;
    private Handler handler;

    public interface OnPlayerListener extends OnSimplePlayerListener {
        void onPlayerAudioSourceDuration(int duration);

        void onPlayerPositionChanged(int position);
    }

    public ProgressPlayer(OnPlayerListener onPlayerListener) {
        this.playerListener = onPlayerListener;
        handler = new Handler();
        checkPositionStateThread = new CheckPositionStateThread();
    }

    @Override
    protected void setOnCompletionListener() {
        mediaPlayer.setOnCompletionListener(this);
    }

    @Override
    public void initInstance(Context context, Uri uri) {
        super.initInstance(context, uri);
        playerListener.onPlayerAudioSourceDuration(mediaPlayer.getDuration());
    }

    @Override
    protected void onError() {
        super.onError();
        playerListener.onPlayerFinished();
    }

    @Override
    public void start() {
        super.start();
        handler.post(checkPositionStateThread);
    }

    @Override
    public void stop() {
        super.stop();
        playerListener.onPlayerFinished();
    }

    public void seekTo(int position) {
        if (isInitInstance()) {
            mediaPlayer.seekTo(position);
        }
    }

    private class CheckPositionStateThread implements Runnable {

        @Override
        public void run() {
            if (isPlaying()) {
                playerListener.onPlayerPositionChanged(mediaPlayer.getCurrentPosition());
                handler.postDelayed(this, CHECK_POSITION_THREAD_DELAY);
            }
        }
    }

}
