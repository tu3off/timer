package com.dmd.timer.media.recorder;

import android.media.MediaRecorder;

import java.io.File;
import java.io.IOException;

public class Recorder implements RecorderController, MediaRecorder.OnInfoListener {

    private static final int MAX_RECORD_DURATION_MS = 6_000;
    private MediaRecorder mediaRecorder;
    private OnRecorderListener recorderListener;

    public interface OnRecorderListener {
        void onRecorderStop();
        void onRecorderFinished();
    }

    public Recorder(OnRecorderListener recorderListener) {
        this.recorderListener = recorderListener;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (isInitInstance()) {
            stop();
        }
    }

    @Override
    public void initInstance(String path) {
        mediaRecorder = new MediaRecorder();
        File outputFile = new File(path);
        if (outputFile.exists()) {
            outputFile.delete();
        }
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mediaRecorder.setOutputFile(path);
        mediaRecorder.setOnInfoListener(this);
        mediaRecorder.setMaxDuration(MAX_RECORD_DURATION_MS);
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() {
        mediaRecorder.start();
    }

    @Override
    public void stop() {
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
        recorderListener.onRecorderStop();
    }

    @Override
    public boolean isInitInstance() {
        return mediaRecorder != null;
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if(what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            recorderListener.onRecorderFinished();
        }
    }

}
