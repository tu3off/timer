package com.dmd.timer.media.player;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import java.io.IOException;

public class Player implements PlayerController, MediaPlayer.OnCompletionListener {

    protected MediaPlayer mediaPlayer;
    private OnSimplePlayerListener simplePlayerListener;

    public interface OnSimplePlayerListener {
        void onPlayerFinished();
    }

    public Player() {
    }

    public Player(OnSimplePlayerListener simplePlayerListener) {
        this.simplePlayerListener = simplePlayerListener;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (isInitInstance()) {
            stop();
        }
    }

    @Override
    public void initInstance(String path) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        setOnCompletionListener();
        try {
            mediaPlayer.prepare();
        } catch (IOException e) {
            onError();
        }
    }

    protected void onError() {
    }

    @Override
    public void initInstance(Context context, Uri uri) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(context, uri);
            setOnCompletionListener();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isPlaying() {
        if (!isInitInstance()) {
            return false;
        }
        return mediaPlayer.isPlaying();
    }

    @Override
    public boolean isInitInstance() {
        return mediaPlayer != null;
    }

    @Override
    public void start() {
        mediaPlayer.start();
    }

    @Override
    public void pause() {
        mediaPlayer.pause();
    }

    @Override
    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        mediaPlayer = null;
        notifyPlayerFinished();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stop();
    }

    protected void setOnCompletionListener() {
        mediaPlayer.setOnCompletionListener(this);
    }

    private void notifyPlayerFinished() {
        if (simplePlayerListener != null) {
            simplePlayerListener.onPlayerFinished();
        }
    }
}
