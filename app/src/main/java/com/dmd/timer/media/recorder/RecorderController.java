package com.dmd.timer.media.recorder;

public interface RecorderController {
    void initInstance(String path);

    boolean isInitInstance();

    void start();

    void stop();
}