package com.dmd.timer.media.player;

import android.content.Context;
import android.net.Uri;

public interface PlayerController {
    void initInstance(String path);

    void initInstance(Context context, Uri uri);

    boolean isInitInstance();

    void start();

    void pause();

    void stop();

    boolean isPlaying();
}