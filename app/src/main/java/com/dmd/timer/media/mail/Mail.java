package com.dmd.timer.media.mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail extends Authenticator {

    private static final String FROM_EMAIL = "andriituzov@gmail.com";
    private static final String FROM_PASSWORD = "mk6J5KdvIG";
    private static final String TO_EMAIL = "tuzknure@gmail.com";
    private static final String EMAIL_PORT = "465";
    private static final String SMTP_AUTH = "true";
    private static final String STARTTLS = "true";
    private static final String FALLBACK = "false";
    private static final String EMAIL_HOST = "smtp.gmail.com";
    private static final String TRANSPORT = "smtp";
    private static final String EMAIL_SUBJECT = "Timer support";
    private static final String PORT_KEY = "mail.smtp.port";
    private static final String AUTH_KEY = "mail.smtp.auth";
    private static final String HOST_KEY = "mail.smtp.host";
    private static final String STARTTLS_KEY = "mail.smtp.starttls.enable";
    private static final String SOCKET_FACTORY_PORT_KEY = "mail.smtp.socketFactory.port";
    private static final String SOCKET_FACTORY_CLASS_KEY = "mail.smtp.socketFactory.class";
    private static final String SOCKET_FACTORY_FALLBACK_KEY = "mail.smtp.socketFactory.fallback";
    private static final String SSL_SOCKET_FACTORY = "javax.net.ssl.SSLSocketFactory";

    private String emailBody;
    private Properties properties;
    private Session session;
    private MimeMessage mimeMessage;

    public Mail(String emailSubject, String emailBody) {
        this.emailBody = generateBody(emailSubject, emailBody);
        properties = new Properties();
        properties.put(PORT_KEY, EMAIL_PORT);
        properties.put(AUTH_KEY, SMTP_AUTH);
        properties.put(HOST_KEY, EMAIL_HOST);
        properties.put(STARTTLS_KEY, STARTTLS);
        properties.put(SOCKET_FACTORY_PORT_KEY, EMAIL_PORT);
        properties.put(SOCKET_FACTORY_CLASS_KEY, SSL_SOCKET_FACTORY);
        properties.put(SOCKET_FACTORY_FALLBACK_KEY, FALLBACK);
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(FROM_EMAIL, FROM_PASSWORD);
    }

    public MimeMessage createEmailMessage() throws Exception {
        session = Session.getInstance(properties, this);
        mimeMessage = new MimeMessage(session);
        mimeMessage.setFrom(new InternetAddress(FROM_EMAIL));
        mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(TO_EMAIL));
        mimeMessage.setSubject(EMAIL_SUBJECT);
        mimeMessage.setText(emailBody);
        return mimeMessage;
    }

    public void sendMessage() throws Exception {
        Transport transport = session.getTransport(TRANSPORT);
        transport.connect(EMAIL_HOST, FROM_EMAIL, FROM_PASSWORD);
        transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
        transport.close();
        Transport.send(mimeMessage);
    }

    private String generateBody(String emailSubject, String emailBody) {
        StringBuilder builder = new StringBuilder();
        builder.append("Email address: ");
        builder.append(emailBody);
        builder.append("\n");
        builder.append("Message: ");
        builder.append(emailSubject);
        return builder.toString();
    }

}
