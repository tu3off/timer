package com.dmd.timer.core;

import android.app.Application;
import android.content.Context;
import android.location.LocationManager;

import com.dmd.timer.core.settings.SharedHelper;
import com.dmd.timer.utils.Launcher;

public class TheApplication extends Application {

    private volatile SharedHelper sharedHelper;
    private volatile LocationManager locationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Launcher.service.startTracker(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Launcher.service.stopTracker(this);
        sharedHelper = null;
        locationManager = null;
    }

    public LocationManager getLocationManager() {
        if(locationManager == null) {
            synchronized (this) {
                if(locationManager == null) {
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                }
            }
        }
        return locationManager;
    }

    public SharedHelper getSharedHelper() {
        if (sharedHelper == null) {
            synchronized (this) {
                if (sharedHelper == null) {
                    sharedHelper = new SharedHelper(this);
                }
            }
        }
        return sharedHelper;
    }

}
