package com.dmd.timer.core.settings;

import android.content.Context;
import android.content.SharedPreferences;

import com.dmd.timer.R;
import com.dmd.timer.utils.NotificationExamples;
import com.dmd.timer.utils.SystemUtils;

public class SharedHelper {

    public static final int TYPE_TEXT = 0;
    public static final int TYPE_VOICE = 1;

    private static final long DEFAULT_TIME = 0;
    private static final float DEFAULT_SPEED = 5.0f * 1000 / 3600;

    private static final String CUSTOM_TEXT_NOTIFICATION = "custom_text_notification";
    private static final String FIRST_START = "first_start";
    private static final String NAME = "app_info";
    private static final String SOUND_PATH = "sound_path";
    private static final String SPEED = "speed";
    private static final String TEXT_NOTIFICATION = "text_notification";
    private static final String TIME = "time";
    private static final String TYPE_NOTIFICATION = "type_notification";
    private static final String USER_AGREEMENT = "user_agreement";

    private Context context;
    private SharedPreferences sharedPreferences;

    public SharedHelper(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public boolean isUserAgreementApproved() {
        return sharedPreferences.getBoolean(USER_AGREEMENT, false);
    }

    public void setUserAgreement(boolean userAgreementApproved) {
        sharedPreferences.edit().putBoolean(USER_AGREEMENT, userAgreementApproved).apply();
    }

    public boolean isFirstStart() {
        return sharedPreferences.getBoolean(FIRST_START, true);
    }

    public void setFirstStart(boolean firstStart) {
        sharedPreferences.edit().putBoolean(FIRST_START, firstStart).apply();
    }

    public float getSpeed() {
        return sharedPreferences.getFloat(SPEED, DEFAULT_SPEED);
    }

    public void setSpeed(float speed) {
        sharedPreferences.edit().putFloat(SPEED, speed).apply();
    }

    public String getTextNotification() {
        return sharedPreferences.getString(TEXT_NOTIFICATION, NotificationExamples.TEXT_NOTIFICATION_EXAMPLE_1);
    }

    public void setTextNotification(String text) {
        sharedPreferences.edit().putString(TEXT_NOTIFICATION, text).apply();
    }

    public int getTypeNotification() {
        return sharedPreferences.getInt(TYPE_NOTIFICATION, TYPE_TEXT);
    }

    public void setTypeNotification(int type) {
        sharedPreferences.edit().putInt(TYPE_NOTIFICATION, type).apply();
    }

    public String getSoundPath() {
        return sharedPreferences.getString(SOUND_PATH,
                SystemUtils.rawResourceToUriString(context, R.raw.sound1));
    }

    public void setSoundPath(String path) {
        sharedPreferences.edit().putString(SOUND_PATH, path).apply();
    }

    public void setTime(int timeMin, int timeSec) {
        long time = timeMin * 60000 + timeSec * 1000;
        sharedPreferences.edit().putLong(TIME, time).apply();
    }

    public long getTime() {
        return sharedPreferences.getLong(TIME, DEFAULT_TIME);
    }

    public void setCustomTextNotification(String customTextNotification) {
        sharedPreferences.edit().putString(CUSTOM_TEXT_NOTIFICATION, customTextNotification).apply();
    }

    public String getCustomTextNotification() {
        return sharedPreferences.getString(CUSTOM_TEXT_NOTIFICATION, null);
    }


}
