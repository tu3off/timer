package com.dmd.timer.ui.activity;

import android.location.LocationManager;

import com.dmd.timer.core.TheApplication;
import com.dmd.timer.core.settings.SharedHelper;

public interface ActivityBridge {
    TheApplication getTheApplication();

    SharedHelper getSharedHelper();

    LocationManager getLocationManager();
}
