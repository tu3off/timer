package com.dmd.timer.ui.activity.home;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DigitalClock;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmd.timer.R;
import com.dmd.timer.service.observer.TimerObserver;
import com.dmd.timer.ui.activity.ServiceActivity;
import com.dmd.timer.ui.customview.AlphaDoubleTapImageView;
import com.dmd.timer.ui.customview.DoubleTapImageView;
import com.dmd.timer.utils.Converter;
import com.dmd.timer.utils.FontsUtils;
import com.dmd.timer.utils.Launcher;
import com.dmd.timer.utils.SystemUtils;

public final class HomeActivity extends ServiceActivity implements TimerObserver {

    private AlphaDoubleTapImageView adtivTimerBot;
    private AlphaDoubleTapImageView adtivTimerTop;
    private boolean activityForResultStarted;
    @SuppressWarnings("deprecation")
    private DigitalClock dcClock;
    private DoubleTapImageView dtivBridge;
    private EditText etTimerMinutes;
    private EditText etTimerSeconds;
    private int min;
    private int sec;
    private ImageView ivTimerCondition;
    private ImageView ivTimerLowerLayer;
    private ImageView ivTimerLowerLine;
    private ImageView ivTimerMenuBot;
    private ImageView ivTimerMenuTop;
    private ImageView ivTimerHigherLine;
    private ImageView ivTipand;
    private TextView tvTimerColon;
    private TimerListener timerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initViews();
        tipandLogic();
    }

    @Override
    public void onBackPressed() {
        if (getSharedHelper().isFirstStart()) {
            closeTipand();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        activityForResultStarted = false;
        if (!isGPSEnabled()) {
            toast(R.string.gps_is_disable);
            finish();
        }
    }

    @Override
    protected void connectionWithServiceHasBeenEstablished() {
        trackerService.addObserver(this);
        if (!isGPSEnabled() && !activityForResultStarted) {
            activityForResultStarted = true;
            Launcher.activity.actionLocationSourceSettings(this);
        }
        long time = trackerService.getTime();
        boolean isTimerFinished = trackerService.isTimerFinished();
        updateTimer(time);
        timerFinished(isTimerFinished);
        if (trackerService.isTrackerStarted()) {
            ivTimerCondition.setAlpha(1.0f);
        }
    }

    @Override
    protected void disconnectFromService() {
        super.disconnectFromService();
        trackerService.removeObserver(this);
    }

    @Override
    public void updateTimer(long time) {
        final String[] minutesAndSeconds = Converter.msToMinAndSecStringArray(time);
        min = Integer.parseInt(minutesAndSeconds[0]);
        sec = Integer.parseInt(minutesAndSeconds[1]);
        etTimerMinutes.setText(minutesAndSeconds[0]);
        etTimerSeconds.setText(minutesAndSeconds[1]);
    }

    @Override
    public void timerFinished(boolean timerFinished) {
        if (timerFinished) {
            timerListener.startAnimTimerConditionOne();
        }
    }

    @SuppressWarnings("deprecation")
    private void initViews() {
        timerListener = new TimerListener();
        adtivTimerTop = (AlphaDoubleTapImageView) findViewById(R.id.adtiv_timer_top);
        adtivTimerBot = (AlphaDoubleTapImageView) findViewById(R.id.adtiv_timer_bot);
        dcClock = (DigitalClock) findViewById(R.id.dc_clock);
        dtivBridge = (DoubleTapImageView) findViewById(R.id.dtiv_bridge);
        etTimerMinutes = (EditText) findViewById(R.id.et_timer_minutes);
        etTimerMinutes.setOnKeyListener(new KeyListener());
        etTimerSeconds = (EditText) findViewById(R.id.et_timer_seconds);
        etTimerSeconds.setOnKeyListener(new KeyListener());
        etTimerMinutes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent ev) {
                etTimerMinutes.requestFocus();
                etTimerMinutes.setActivated(true);
                etTimerMinutes.setSelection(0);
                etTimerMinutes.setText("");
                SystemUtils.showKeyboard(getApplicationContext());
                return true;
            }
        });
        ivTimerCondition = (ImageView) findViewById(R.id.iv_timer_condition);
        ivTimerHigherLine = (ImageView) findViewById(R.id.iv_timer_higher_line);
        ivTimerLowerLayer = (ImageView) findViewById(R.id.iv_timer_lower_layer);
        ivTimerLowerLine = (ImageView) findViewById(R.id.iv_timer_lower_line);
        ivTimerMenuBot = (ImageView) findViewById(R.id.iv_timer_bot_menu);
        ivTimerMenuTop = (ImageView) findViewById(R.id.iv_timer_top_menu);
        tvTimerColon = (TextView) findViewById(R.id.tv_timer_colon);
        etTimerMinutes.setEnabled(false);
        etTimerSeconds.setEnabled(false);
        ivTimerLowerLayer.setOnClickListener(timerListener);
        adtivTimerBot.setOnClickListener(timerListener);
        adtivTimerBot.setOnTapListener(timerListener);
        adtivTimerTop.setOnClickListener(timerListener);
        adtivTimerTop.setOnTapListener(timerListener);
        dtivBridge.setOnTapListener(timerListener);
        ivTimerMenuBot.setOnClickListener(timerListener);
        ivTimerMenuBot.setClickable(false);
        ivTimerMenuTop.setOnClickListener(timerListener);
        ivTimerMenuTop.setClickable(false);
        FontsUtils.setGothic(getAssets(), etTimerMinutes);
        FontsUtils.setGothic(getAssets(), etTimerSeconds);
        FontsUtils.setGothic(getAssets(), tvTimerColon);
        FontsUtils.setGothic(getAssets(), (TextView) findViewById(R.id.tv_min));
        FontsUtils.setGothic(getAssets(), (TextView) findViewById(R.id.tv_sec));
        FontsUtils.setGothic(getAssets(), dcClock);
    }

    private void tipandLogic() {
        if (getSharedHelper().isFirstStart()) {
            ivTipand = (ImageView) findViewById(R.id.iv_tipand);
            ivTipand.setImageResource(R.drawable.tipandnew);
            ivTipand.setVisibility(View.VISIBLE);
            ivTipand.setOnClickListener(new TipandClickListener());
        }
    }

    private void closeTipand() {
        ivTipand.setVisibility(View.GONE);
        ivTipand = null;
        getSharedHelper().setFirstStart(false);
    }

    private void checkTimer() {
        if (etTimerMinutes.getText().toString().equals("")) {
            etTimerMinutes.setText("00");
        }
        if (etTimerSeconds.getText().toString().equals("")) {
            etTimerSeconds.setText("00");
        }
        min = checkValue(Integer.parseInt(etTimerMinutes.getText().toString()), etTimerMinutes);
        sec = checkValue(Integer.parseInt(etTimerSeconds.getText().toString()), etTimerSeconds);
    }

    private int checkValue(int val, EditText editText) {
        if (val < 10 && !editText.getText().toString().equals("00")) {
            editText.getText().insert(0, "0");
        }
        if (val > 60) {
            editText.setText("59");
            val = 59;
        }
        return val;
    }

    private final class TimerListener implements View.OnClickListener, DoubleTapImageView.OnTapListener {

        private static final int ANIMATION_DURATION = 350;
        private AnimatorSet expandSet;
        private AnimatorSet collapseSet;
        private boolean expanded;
        private ObjectAnimator collapseBotAnim;
        private ObjectAnimator collapseBotLineAnim;
        private ObjectAnimator collapseMicIconAnim;
        private ObjectAnimator collapseSettingsIconAnim;
        private ObjectAnimator collapseTopAnim;
        private ObjectAnimator collapseTopLineAnim;
        private ObjectAnimator expandBotAnim;
        private ObjectAnimator expandBotLineAnim;
        private ObjectAnimator expandMicIconAnim;
        private ObjectAnimator expandSettingsIconAnim;
        private ObjectAnimator expandTopAnim;
        private ObjectAnimator expandTopLineAnim;
        private ValueAnimator timerConditionOneAnim;
        private ValueAnimator timerConditionTwoAnim;

        @Override
        public void onClick(View v) {
            checkTimer();
            getSharedHelper().setTime(min, sec);
            trackerService.updateTime();
            switch (v.getId()) {
                case R.id.iv_timer_top_menu:
                    Launcher.activity.notificationVoiceRecord(HomeActivity.this);
                    break;
                case R.id.iv_timer_lower_layer:
                    expandWithAnimation();
                    break;
                case R.id.adtiv_timer_bot:
                case R.id.adtiv_timer_top:
                    break;
                case R.id.iv_timer_bot_menu:
                    Launcher.activity.notificationSettings(HomeActivity.this);
                    break;
            }
        }

        @Override
        public void OnDoubleTap() {
            if (!trackerService.isTrackerStarted()) {
                imageAnimate();
            }
        }

        @Override
        public void OnSingleTap() {
            expandWithAnimation();
        }

        private void expandWithAnimation() {
            if (!expanded) {
                if (trackerService.isTrackerStarted()) {
                    startAnimTimerConditionOne();
                    trackerService.stopForegroundTracker();
                } else {
                    startAnimTimerConditionTwo();
                    trackerService.startForegroundTracker();
                }
            } else {
                collapse();
            }
        }

        private void imageAnimate() {
            if (!expanded) {
                expand();
            } else {
                collapse();
            }
        }

        private void expand() {
            initExpandAndCollapseAnimation();
            if (collapseSet.isRunning()) {
                collapseSet.cancel();
            }
            expandSet.start();
            expanded = true;
        }

        private void collapse() {
            if (expandSet.isRunning()) {
                expandSet.cancel();
            }
            collapseSet.start();
            expanded = false;
            SystemUtils.hideKeyboard(getApplicationContext(), etTimerMinutes);
            SystemUtils.hideKeyboard(getApplicationContext(), etTimerSeconds);
            checkTimer();
            getSharedHelper().setTime(min, sec);
            trackerService.updateTime();
        }

        private void initExpandAndCollapseAnimation() {
            if (!isExpandAndCollapseInitiated()) {
                int translation = ivTimerLowerLayer.getHeight() / 2;
                expandTopAnim = ObjectAnimator.ofFloat(adtivTimerTop, "translationY", -translation);
                expandBotAnim = ObjectAnimator.ofFloat(adtivTimerBot, "translationY", translation);
                expandMicIconAnim = ObjectAnimator.ofFloat(ivTimerMenuTop, "alpha", 1);
                expandSettingsIconAnim = ObjectAnimator.ofFloat(ivTimerMenuBot, "alpha", 1);
                collapseTopAnim = ObjectAnimator.ofFloat(adtivTimerTop, "translationY", 0f);
                collapseBotAnim = ObjectAnimator.ofFloat(adtivTimerBot, "translationY", 0f);
                collapseMicIconAnim = ObjectAnimator.ofFloat(ivTimerMenuTop, "alpha", 0);
                collapseSettingsIconAnim = ObjectAnimator.ofFloat(ivTimerMenuBot, "alpha", 0);
                expandTopLineAnim = ObjectAnimator.ofFloat(ivTimerHigherLine, "translationY", -translation);
                expandBotLineAnim = ObjectAnimator.ofFloat(ivTimerLowerLine, "translationY", translation);
                collapseTopLineAnim = ObjectAnimator.ofFloat(ivTimerHigherLine, "translationY", 0f);
                collapseBotLineAnim = ObjectAnimator.ofFloat(ivTimerLowerLine, "translationY", 0f);
                collapseSet = new AnimatorSet();
                collapseSet.setDuration(ANIMATION_DURATION);
                collapseSet.playTogether(collapseTopAnim, collapseBotAnim, collapseMicIconAnim, collapseSettingsIconAnim, collapseBotLineAnim, collapseTopLineAnim);
                collapseSet.addListener(new CollapseAnimationListener());
                expandSet = new AnimatorSet();
                expandSet.setDuration(ANIMATION_DURATION);
                expandSet.playTogether(expandTopAnim, expandBotAnim, expandMicIconAnim, expandSettingsIconAnim, expandBotLineAnim, expandTopLineAnim);
                expandSet.addListener(new ExpandAnimationListener());
            }
        }

        private boolean isExpandAndCollapseInitiated() {
            if (expandBotAnim == null || expandTopAnim == null || expandMicIconAnim == null
                    || expandSettingsIconAnim == null || collapseTopAnim == null
                    || collapseBotAnim == null || collapseMicIconAnim == null
                    || collapseSettingsIconAnim == null || expandTopLineAnim == null
                    || expandBotLineAnim == null || collapseBotLineAnim == null
                    || collapseTopLineAnim == null) {
                return false;
            }
            return true;
        }

        public void startAnimTimerConditionOne() {
            if (timerConditionTwoAnim != null) {
                timerConditionTwoAnim.setRepeatCount(0);
                timerConditionTwoAnim.cancel();
            }
            if (timerConditionOneAnim == null) {
                timerConditionOneAnim = ObjectAnimator.ofFloat(ivTimerCondition, View.ALPHA, ivTimerCondition.getAlpha(), 0);
                timerConditionOneAnim.setDuration(1000);
            } else {
                timerConditionOneAnim.setFloatValues(ivTimerCondition.getAlpha(), 0);
            }
            timerConditionOneAnim.start();

        }

        public void startAnimTimerConditionTwo() {
            if (timerConditionTwoAnim == null) {
                timerConditionTwoAnim = ObjectAnimator.ofFloat(ivTimerCondition, View.ALPHA, 0, 1);
                timerConditionTwoAnim.setDuration(2000);
                timerConditionTwoAnim.setRepeatMode(ValueAnimator.REVERSE);
            }
            timerConditionTwoAnim.setRepeatCount(ValueAnimator.INFINITE);
            timerConditionTwoAnim.start();
        }


    }

    private final class ExpandAnimationListener implements Animator.AnimatorListener {

        @Override
        public void onAnimationStart(Animator animation) {
            ivTimerHigherLine.setVisibility(View.VISIBLE);
            ivTimerLowerLine.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            ivTimerMenuBot.setClickable(true);
            ivTimerMenuTop.setClickable(true);
            etTimerMinutes.setEnabled(true);
            etTimerSeconds.setEnabled(true);
            etTimerMinutes.setFocusableInTouchMode(true);
            etTimerSeconds.setFocusableInTouchMode(true);
            dtivBridge.setVisibility(View.GONE);
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    }

    private final class CollapseAnimationListener implements Animator.AnimatorListener {

        @Override
        public void onAnimationStart(Animator animation) {
            ivTimerMenuBot.setClickable(false);
            ivTimerMenuTop.setClickable(false);
            etTimerMinutes.setEnabled(false);
            etTimerSeconds.setEnabled(false);
            dtivBridge.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            ivTimerHigherLine.setVisibility(View.INVISIBLE);
            ivTimerLowerLine.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    }

    private final class TipandClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            closeTipand();
        }
    }


    private void setSecondFocus() {
        etTimerSeconds.requestFocus();
        etTimerSeconds.setActivated(true);
        etTimerSeconds.setSelection(0);
        etTimerSeconds.setText("");
    }

    private final class KeyListener implements View.OnKeyListener {
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (v.getId() == R.id.et_timer_minutes) {
                if (etTimerMinutes.getText().length() == 2) {
                    checkTimer();
                    setSecondFocus();
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        SystemUtils.hideKeyboard(getApplicationContext(), v);
                    }
                    return true;
                } else {
                    return false;
                }
            }
            if (v.getId() == R.id.et_timer_seconds) {
                if (etTimerSeconds.getText().length() == 2) {
                    checkTimer();
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        SystemUtils.hideKeyboard(getApplicationContext(), v);
                    }
                    return true;
                } else {
                    return false;
                }
            }
            return true;

        }
    }
}
