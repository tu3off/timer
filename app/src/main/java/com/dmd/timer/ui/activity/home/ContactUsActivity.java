package com.dmd.timer.ui.activity.home;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dmd.timer.R;
import com.dmd.timer.media.mail.Mail;
import com.dmd.timer.ui.activity.ToolbarActivity;
import com.dmd.timer.utils.FontsUtils;
import com.dmd.timer.utils.Validator;

public class ContactUsActivity extends ToolbarActivity {

    private Button btSendMessage;
    private EditText etInputEmail;
    private EditText etInputMessage;
    private SendMessage sendMessage;

    @Override
    protected int contentViewLayoutId() {
        return R.layout.activity_contact_us;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle(R.string.title_contact_us);
        homeAsUp();
        initViews();
    }

    private void initViews() {
        sendMessage = new SendMessage();
        btSendMessage = (Button) findViewById(R.id.bt_send_message);
        etInputEmail = (EditText) findViewById(R.id.et_input_email);
        etInputMessage = (EditText) findViewById(R.id.et_input_message);
        btSendMessage.setOnClickListener(sendMessage);
        FontsUtils.setGothic(getAssets(), etInputEmail);
        FontsUtils.setGothic(getAssets(), etInputMessage);
        FontsUtils.setGothicBold(getAssets(), btSendMessage);
    }

    private class SendMessage extends AsyncTask<Mail, Void, Void> implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            sendMessage();
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog(R.string.send_email, R.string.message_is_sending);
        }

        @Override
        protected Void doInBackground(Mail... params) {
            Mail mail = params[0];
            try {
                mail.createEmailMessage();
                mail.sendMessage();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void o) {
            dismissProgressDialog();
            finish();
        }

        private void sendMessage() {
            etInputMessage.setError(null);
            etInputEmail.setError(null);
            String message = etInputMessage.getText().toString();
            String email = etInputEmail.getText().toString();
            boolean validEmail = Validator.isValidEmail(email);
            boolean emptyMessage = TextUtils.isEmpty(message);
            if (validEmail && !emptyMessage) {
                execute(new Mail(email, message));
            } else {
                if (!validEmail) {
                    etInputEmail.setError(getString(R.string.invalid_email));
                }
                if (emptyMessage) {
                    etInputMessage.setError(getString(R.string.invalid_message));
                }
            }
        }

    }
}
