package com.dmd.timer.ui.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.dmd.timer.service.TrackerService;

public abstract class ServiceActivity extends BaseActivity implements ServiceConnection {

    protected TrackerService trackerService;

    @Override
    protected void onStart() {
        super.onStart();
        Intent bindTrackerService = new Intent(this, TrackerService.class);
        bindService(bindTrackerService, this, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (trackerService != null) {
            disconnectFromService();
            unbindService(this);
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        TrackerService.TrackerBinder binder = (TrackerService.TrackerBinder) service;
        trackerService = binder.getService();
        connectionWithServiceHasBeenEstablished();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        trackerService = null;
    }

    protected void connectionWithServiceHasBeenEstablished() {
    }

    protected void disconnectFromService() {
    }

}
