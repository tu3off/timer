package com.dmd.timer.ui.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmd.timer.R;
import com.dmd.timer.model.Sound;
import com.dmd.timer.utils.FontsUtils;

import java.util.ArrayList;
import java.util.List;

public class SoundNotificationAdapter extends BaseAdapter {

    private List<Sound> soundNotificationList;
    private LayoutInflater inflater;
    private Context context;

    public SoundNotificationAdapter(Context context) {
        this.context = context;
        soundNotificationList = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    public void addSoundNotifications(List<Sound> soundNotificationList) {
        this.soundNotificationList.clear();
        this.soundNotificationList.addAll(soundNotificationList);
        notifyDataSetChanged();
    }

    public int positionByPath(String path) {
        int size = soundNotificationList.size();
        for (int i = 0; i < size; i++) {
            if (path.equals(soundNotificationList.get(i).getPath())) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int getCount() {
        return soundNotificationList.size();
    }

    @Override
    public Sound getItem(int position) {
        return soundNotificationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Sound example = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_sound_notification, parent, false);
            viewHolder = new ViewHolder(convertView, context.getAssets());
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.setTextNotificationExample(example);
        return convertView;
    }

    private static class ViewHolder {

        private TextView tvTextNotification;

        public ViewHolder(View view, AssetManager assetManager) {
            tvTextNotification = (TextView) view.findViewById(R.id.tv_sound_notification);
            FontsUtils.setGothic(assetManager, tvTextNotification);
        }

        public void setTextNotificationExample(Sound example) {
            tvTextNotification.setText(example.getTitle());
        }

    }
}
