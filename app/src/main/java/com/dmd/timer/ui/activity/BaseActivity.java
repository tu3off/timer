package com.dmd.timer.ui.activity;

import android.app.ProgressDialog;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.dmd.timer.core.settings.SharedHelper;
import com.dmd.timer.core.TheApplication;

public abstract class BaseActivity extends AppCompatActivity implements ActivityBridge {

    private LocationManager locationManager;
    private ProgressDialog progressDialog;
    private SharedHelper sharedHelper;
    private TheApplication theApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theApplication = (TheApplication) getApplicationContext();
        sharedHelper = theApplication.getSharedHelper();
        locationManager = theApplication.getLocationManager();
    }

    @Override
    public SharedHelper getSharedHelper() {
        return sharedHelper;
    }

    @Override
    public TheApplication getTheApplication() {
        return theApplication;
    }

    @Override
    public LocationManager getLocationManager() {
        return locationManager;
    }

    protected boolean isGPSEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    protected void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void toast(int resId) {
        toast(getString(resId));
    }

    protected void showProgressDialog(String title, String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    protected void showProgressDialog(int titleResId, int messageResId) {
        this.showProgressDialog(getString(titleResId), getString(messageResId));
    }

    protected void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
