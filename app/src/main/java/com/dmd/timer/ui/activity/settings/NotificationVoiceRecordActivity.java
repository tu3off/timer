package com.dmd.timer.ui.activity.settings;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dmd.timer.R;
import com.dmd.timer.media.player.ProgressPlayer;
import com.dmd.timer.media.recorder.Recorder;
import com.dmd.timer.ui.activity.ToolbarActivity;
import com.dmd.timer.utils.FontsUtils;
import com.dmd.timer.utils.SystemUtils;

import java.io.File;

public class NotificationVoiceRecordActivity extends ToolbarActivity {

    public static final String PATH_BUNDLE_KEY = "notification.voice.record.activity.bundle.path.key";

    private CheckBox cbPlayer;
    private CheckBox cbRecorder;
    private PlayerListener playerListener;
    private SeekBar sbPlayerProgress;
    private TextView tvMakeNotificationRecord;
    private TextView tvListenToCurrentNotification;

    @Override
    protected int contentViewLayoutId() {
        return R.layout.activity_notification_voice_record;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle(R.string.title_notification_voice);
        homeAsUp();
        String sound = getIntent().getStringExtra(PATH_BUNDLE_KEY);
        playerListener = new PlayerListener(sound, SystemUtils.pathToTempFile(getTheApplication()));
        initViews();
        if(sound != null) {
            cbPlayer.performClick();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        playerListener.stopMedia();
    }

    private void initViews() {
        cbPlayer = (CheckBox) findViewById(R.id.cb_player_play);
        cbRecorder = (CheckBox) findViewById(R.id.cb_record);
        sbPlayerProgress = (SeekBar) findViewById(R.id.sb_player_progress);
        tvMakeNotificationRecord = (TextView) findViewById(R.id.tv_make_notification_record);
        tvListenToCurrentNotification = (TextView) findViewById(R.id.tv_listen_to_current_record);
        sbPlayerProgress.setOnSeekBarChangeListener(playerListener);
        cbPlayer.setOnClickListener(playerListener);
        cbRecorder.setOnClickListener(playerListener);
        FontsUtils.setGothic(getAssets(), tvMakeNotificationRecord);
        FontsUtils.setGothic(getAssets(), tvListenToCurrentNotification);
    }

    private class PlayerListener implements View.OnClickListener,
            ProgressPlayer.OnPlayerListener, SeekBar.OnSeekBarChangeListener, Recorder.OnRecorderListener {

        private ProgressPlayer player;
        private Recorder recorder;
        private String recorderPath;
        private String playerPath;

        public PlayerListener(String playerPath, String recorderPath) {
            this.recorderPath = recorderPath;
            if (playerPath != null) {
                this.playerPath = playerPath;
            } else {
                this.playerPath = recorderPath;
            }
            player = new ProgressPlayer(this);
            recorder = new Recorder(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cb_record:
                    recorder();
                    break;
                case R.id.cb_player_play:
                    player();
                    break;
            }
        }

        @Override
        public void onPlayerAudioSourceDuration(int duration) {
            sbPlayerProgress.setMax(duration);
        }

        @Override
        public void onPlayerPositionChanged(int position) {
            sbPlayerProgress.setProgress(position);
        }

        @Override
        public void onPlayerFinished() {
            sbPlayerProgress.setProgress(0);
            cbPlayer.setChecked(false);
        }

        @Override
        public void onRecorderStop() {
            cbRecorder.setChecked(false);
        }

        @Override
        public void onRecorderFinished() {
            cbRecorder.setChecked(false);
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (player.isInitInstance() && fromUser) {
                player.seekTo(progress);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

        private void player() {
            if (player.isPlaying()) {
                player.pause();
            } else {
                if (recorder.isInitInstance()) {
                    recorder.stop();
                }
                if (!player.isInitInstance()) {
                    player.initInstance(getApplicationContext(), Uri.parse(playerPath));
                }
                player.start();
            }
        }

        private void recorder() {
            if (player.isInitInstance()) {
                player.stop();
            }
            if (recorder.isInitInstance()) {
                recorder.stop();
            } else {
                playerPath = recorderPath;
                recorder.initInstance(recorderPath);
                recorder.start();
            }
        }

        public void stopMedia() {
            if (player.isPlaying()) {
                player.stop();
            }
            if (recorder.isInitInstance()) {
                recorder.stop();
            }
        }

    }
}
