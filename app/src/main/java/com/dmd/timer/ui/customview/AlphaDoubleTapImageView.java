package com.dmd.timer.ui.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class AlphaDoubleTapImageView extends DoubleTapImageView {

    private static final int MAX_ALPHA = 255;
    private Matrix matrix;
    private Bitmap bitmap;

    public AlphaDoubleTapImageView(Context context) {
        super(context);
    }

    public AlphaDoubleTapImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlphaDoubleTapImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        matrix = new Matrix();
        getImageMatrix().invert(matrix);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (bitmap == null) {
            bitmap = ((BitmapDrawable) getDrawable()).getBitmap();
        }
        float[] touchPoint = new float[]{event.getX(), event.getY()};
        matrix.mapPoints(touchPoint);
        int x = (int) touchPoint[0];
        int y = (int) touchPoint[1];
        int touchedRG;
        try {
            touchedRG = bitmap.getPixel(x, y);
        } catch (IllegalArgumentException e) {
            return true;
        }
        int alpha = Color.alpha(touchedRG);
        if (alpha == MAX_ALPHA) {
            return gestureDetector.onTouchEvent(event);
        }
        return true;
    }
}
