package com.dmd.timer.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;

public class DoubleTapImageView extends ImageView {

    protected GestureDetector gestureDetector;
    private OnTapListener onTapListener;

    public interface OnTapListener {
        void OnDoubleTap();

        void OnSingleTap();
    }

    public DoubleTapImageView(Context context) {
        super(context);
        initInstance();
    }

    public DoubleTapImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInstance();
    }

    public DoubleTapImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInstance();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    private void initInstance() {
        gestureDetector = new GestureDetector(getContext(), new GestureListener());
    }

    public void setOnTapListener(OnTapListener onTapListener) {
        this.onTapListener = onTapListener;
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (onTapListener != null) {
                onTapListener.OnDoubleTap();
            }
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (onTapListener != null) {
                onTapListener.OnSingleTap();
            }
            return super.onSingleTapConfirmed(e);
        }
    }

}
