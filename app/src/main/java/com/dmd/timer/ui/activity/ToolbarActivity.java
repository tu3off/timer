package com.dmd.timer.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmd.timer.R;
import com.dmd.timer.utils.FontsUtils;

public abstract class ToolbarActivity extends BaseActivity {

    private TextView tvToolbarTitle;
    private ImageView ivHomeAsUp;

    protected abstract int contentViewLayoutId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(contentViewLayoutId());
        initToolbar();
    }

    protected void homeAsUp() {
        ivHomeAsUp = (ImageView) findViewById(R.id.iv_home_as_up);
        ivHomeAsUp.setVisibility(View.VISIBLE);
        ivHomeAsUp.setOnClickListener(new HomeAsUp());
    }

    protected void setToolbarTitle(int stringResId) {
        tvToolbarTitle.setText(getString(stringResId));
        FontsUtils.setGothicBold(getAssets(), tvToolbarTitle);
    }

    private void initToolbar() {
        tvToolbarTitle = (TextView) findViewById(R.id.tv_title);
    }

    private class HomeAsUp implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    }

}
