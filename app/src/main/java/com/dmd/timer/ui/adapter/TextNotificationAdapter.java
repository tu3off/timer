package com.dmd.timer.ui.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmd.timer.R;
import com.dmd.timer.utils.FontsUtils;

import java.util.ArrayList;
import java.util.List;

public class TextNotificationAdapter extends BaseAdapter {

    private static final int NOTIFICATION_EXAMPLES_SIZE = 4;

    private List<String> textNotificationList;
    private LayoutInflater inflater;
    private Context context;

    public TextNotificationAdapter(Context context) {
        this.context = context;
        textNotificationList = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    public void addCustomTextNotification(String textNotification) {
        if (textNotificationList.size() > NOTIFICATION_EXAMPLES_SIZE) {
            textNotificationList.remove(textNotificationList.size() - 1);
        }
        textNotificationList.add(textNotification);
        notifyDataSetChanged();
    }

    public void addTextNotification(List<String> textNotificationList) {
        this.textNotificationList.addAll(textNotificationList);
        notifyDataSetChanged();
    }

    public int positionByString(String textNotification) {
        return textNotificationList.indexOf(textNotification) + 1;
    }

    @Override
    public int getCount() {
        return textNotificationList.size();
    }

    @Override
    public String getItem(int position) {
        return textNotificationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        String example = getItem(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_text_notification, parent, false);
            viewHolder = new ViewHolder(convertView, context.getAssets());
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.setTextNotificationExample(example);
        return convertView;
    }

    private static class ViewHolder {

        private TextView cbTextNotification;

        public ViewHolder(View view, AssetManager assetManager) {
            cbTextNotification = (TextView) view.findViewById(R.id.tv_text_notification);
            FontsUtils.setGothic(assetManager, cbTextNotification);
        }

        public void setTextNotificationExample(String example) {
            cbTextNotification.setText(example);
        }

    }
}
