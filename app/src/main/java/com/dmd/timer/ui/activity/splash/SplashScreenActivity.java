package com.dmd.timer.ui.activity.splash;

import android.os.Bundle;
import android.os.Handler;

import com.dmd.timer.R;
import com.dmd.timer.ui.activity.ServiceActivity;
import com.dmd.timer.ui.activity.ToolbarActivity;
import com.dmd.timer.utils.Launcher;


public class SplashScreenActivity extends ServiceActivity {

    private static final int DELAY_MILLIS = 2_000;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void connectionWithServiceHasBeenEstablished() {
        super.connectionWithServiceHasBeenEstablished();
        if(trackerService.isTrackerStarted()) {
            finish();
            Launcher.activity.userAgreement(SplashScreenActivity.this);
        } else {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    Launcher.activity.userAgreement(SplashScreenActivity.this);
                }
            }, DELAY_MILLIS);
        }
    }
}
