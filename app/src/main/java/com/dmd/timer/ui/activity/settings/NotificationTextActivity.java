package com.dmd.timer.ui.activity.settings;

import android.os.Bundle;
import android.widget.TextView;

import com.dmd.timer.R;
import com.dmd.timer.ui.activity.ToolbarActivity;
import com.dmd.timer.utils.FontsUtils;

public class NotificationTextActivity extends ToolbarActivity {

    private TextView tvTextNotification;

    @Override
    protected int contentViewLayoutId() {
        return R.layout.activity_notification_text;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvTextNotification =  (TextView) findViewById(R.id.tv_notification_text);
        String notificationText = getSharedHelper().getTextNotification();
        tvTextNotification.setText(notificationText);
        setToolbarTitle(R.string.title_notification_text);
        homeAsUp();
        FontsUtils.setGothic(getAssets(), tvTextNotification);
    }

}
