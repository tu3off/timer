package com.dmd.timer.ui.activity.useragreement;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dmd.timer.R;
import com.dmd.timer.ui.activity.ToolbarActivity;
import com.dmd.timer.utils.FontsUtils;
import com.dmd.timer.utils.Launcher;

public class UserAgreementActivity extends ToolbarActivity {

    private Button btConfirm;
    private ConfirmUserAgreementTermsListener confirmUserAgreementTermsListener;
    private CheckBox cbConfirmUserAgreementTerms;
    private TextView tvUserAgreementTerms;

    @Override
    protected int contentViewLayoutId() {
        return R.layout.actiivty_user_agreement;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!getSharedHelper().isUserAgreementApproved()) {
            setToolbarTitle(R.string.title_user_agreement);
            initViews();
        } else {
            nextScreen();
            finish();
        }
    }

    private void initViews() {
        confirmUserAgreementTermsListener = new ConfirmUserAgreementTermsListener();
        btConfirm = (Button) findViewById(R.id.bt_confirm);
        cbConfirmUserAgreementTerms = (CheckBox) findViewById(R.id.cb_confirm_user_agreement_terms);
        tvUserAgreementTerms = (TextView) findViewById(R.id.tv_user_agreement_terms);
        btConfirm.setOnClickListener(confirmUserAgreementTermsListener);
        cbConfirmUserAgreementTerms.setOnCheckedChangeListener(confirmUserAgreementTermsListener);
        FontsUtils.setGothic(getAssets(), tvUserAgreementTerms);
        FontsUtils.setGothic(getAssets(), cbConfirmUserAgreementTerms);
        FontsUtils.setGothic(getAssets(), btConfirm);
    }

    private void nextScreen() {
        Launcher.activity.home(UserAgreementActivity.this);
        finish();
    }

    private class ConfirmUserAgreementTermsListener implements CompoundButton.OnCheckedChangeListener,
            View.OnClickListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                btConfirm.setEnabled(true);
            } else {
                btConfirm.setEnabled(false);
            }
        }

        @Override
        public void onClick(View v) {
            if (cbConfirmUserAgreementTerms.isEnabled()) {
                getSharedHelper().setUserAgreement(true);
                nextScreen();
            }
        }
    }

}
