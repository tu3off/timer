package com.dmd.timer.ui.activity.settings;

import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmd.timer.R;
import com.dmd.timer.core.settings.SharedHelper;
import com.dmd.timer.media.player.Player;
import com.dmd.timer.model.Sound;
import com.dmd.timer.ui.activity.ToolbarActivity;
import com.dmd.timer.ui.adapter.SoundNotificationAdapter;
import com.dmd.timer.ui.adapter.TextNotificationAdapter;
import com.dmd.timer.utils.Converter;
import com.dmd.timer.utils.FontsUtils;
import com.dmd.timer.utils.Launcher;
import com.dmd.timer.utils.NotificationExamples;
import com.dmd.timer.utils.SystemUtils;

import java.util.ArrayList;
import java.util.List;

public final class NotificationSettingsActivity extends ToolbarActivity {

    private static final int MAX_SEEK_BAR = 15;
    private static final int MIN_SEEK_BAR = 5;
    private static final int PERCENT = 100;
    private static final String VOICE_NOTIFICATION_EXAMPLE = "SOUND NAME";

    private CheckBox cbPlayNotification;
    private EditText etCustomNotification;
    private ImageView ivSubmitCustomNotification;
    private int ratio;
    private int soundNotificationPosition;
    private int textNotificationPosition;
    private ListView lvRoot;
    private Player player;
    private RadioButton rbTextNotification;
    private RadioButton rbVoiceNotification;
    private RadioGroup rgNotificationType;
    private RelativeLayout rlCustom;
    private SeekBar sbActivationSpeed;
    private SoundNotificationAdapter soundNotificationAdapter;
    private Spinner spinnerNotificationSound;
    private TextView tvContactUs;
    private TextView tvKmPerHoursValue;
    private TextNotificationAdapter textNotificationAdapter;
    private TextNotificationAdapter emptyAdapter;

    @Override
    protected int contentViewLayoutId() {
        return R.layout.activity_notification_settings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle(R.string.title_notification_settings);
        homeAsUp();
        initListeners();
        initViews();
        initInstance();
        initAdaptersAndAdapterState();
        getSupportActionBar();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopPlayer();
    }

    private void initViews() {
        lvRoot = (ListView) findViewById(R.id.lv_root);
        lvRoot.setOnItemClickListener(new ListViewNotificationTextListener());
        generateExamples();
        generateSoundExamples();
        addHeader();
        addFooter();
    }

    private void initInstance() {
        int speed = Converter.inKmPerHours(getSharedHelper().getSpeed());
        String notification = getSharedHelper().getTextNotification();
        String customTextNotification = getSharedHelper().getCustomTextNotification();
        String path = getSharedHelper().getSoundPath();
        if (customTextNotification != null) {
            textNotificationAdapter.addCustomTextNotification(customTextNotification);
        }
        tvKmPerHoursValue.setText(String.valueOf(speed));
        sbActivationSpeed.setProgress(valueToPercent(speed));
        textNotificationPosition = textNotificationAdapter.positionByString(notification);
        soundNotificationPosition = soundNotificationAdapter.positionByPath(path);
    }

    private void initListeners() {
        textNotificationAdapter = new TextNotificationAdapter(this);
        soundNotificationAdapter = new SoundNotificationAdapter(this);
    }

    private void initAdaptersAndAdapterState() {
        int notificationType = getSharedHelper().getTypeNotification();
        emptyAdapter = new TextNotificationAdapter(getApplicationContext());
        if (notificationType == SharedHelper.TYPE_TEXT) {
            rbTextNotification.setChecked(true);
            rlCustom.setVisibility(View.VISIBLE);
            lvRoot.setAdapter(textNotificationAdapter);
            lvRoot.setItemChecked(textNotificationPosition, true);
        } else {
            rbVoiceNotification.setChecked(true);
            rlCustom.setVisibility(View.GONE);
            lvRoot.setAdapter(emptyAdapter);
        }
        spinnerNotificationSound.setAdapter(soundNotificationAdapter);
        spinnerNotificationSound.setSelection(soundNotificationPosition);

    }

    private void generateExamples() {
        List<String> textNotificationList = new ArrayList<>();
        textNotificationList.add(NotificationExamples.TEXT_NOTIFICATION_EXAMPLE_1);
        textNotificationList.add(NotificationExamples.TEXT_NOTIFICATION_EXAMPLE_2);
        textNotificationList.add(NotificationExamples.TEXT_NOTIFICATION_EXAMPLE_3);
        textNotificationList.add(NotificationExamples.TEXT_NOTIFICATION_EXAMPLE_4);
        textNotificationAdapter.addTextNotification(textNotificationList);
    }

    private void generateSoundExamples() {
        List<Sound> soundList = new ArrayList<>();
        soundList.add(new Sound(VOICE_NOTIFICATION_EXAMPLE + 1,
                SystemUtils.rawResourceToUriString(getApplicationContext(), R.raw.sound1)));
        soundList.add(new Sound(VOICE_NOTIFICATION_EXAMPLE + 2,
                SystemUtils.rawResourceToUriString(getApplicationContext(), R.raw.sound2)));
        soundList.add(new Sound(VOICE_NOTIFICATION_EXAMPLE + 3,
                SystemUtils.rawResourceToUriString(getApplicationContext(), R.raw.sound3)));
        soundList.add(new Sound(VOICE_NOTIFICATION_EXAMPLE + 4,
                SystemUtils.rawResourceToUriString(getApplicationContext(), R.raw.sound4)));
        soundNotificationAdapter.addSoundNotifications(soundList);
    }

    private void addHeader() {
        View header = getLayoutInflater().inflate(R.layout.settings_header, null, false);
        final NotificationTypeListener notificationTypeListener = new NotificationTypeListener();
        rbTextNotification = (RadioButton) header.findViewById(R.id.rb_text_notification);
        rbVoiceNotification = (RadioButton) header.findViewById(R.id.rb_voice_notification);
        rgNotificationType = (RadioGroup) header.findViewById(R.id.rg_notification_type);
        rgNotificationType.setOnCheckedChangeListener(notificationTypeListener);
        rbVoiceNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Launcher.activity.notificationVoiceRecord(NotificationSettingsActivity.this);
            }
        });
        initHeaderFonts();
        lvRoot.addHeaderView(header);
    }

    private void addFooter() {
        View footer = getLayoutInflater().inflate(R.layout.settings_footer, null, false);
        cbPlayNotification = (CheckBox) footer.findViewById(R.id.cb_play_notification);
        etCustomNotification = (EditText) footer.findViewById(R.id.et_custom_notification);
        ivSubmitCustomNotification = (ImageView) footer.findViewById(R.id.iv_submit_custom_notification);
        sbActivationSpeed = (SeekBar) footer.findViewById(R.id.sb_activation_speed);
        spinnerNotificationSound = (Spinner) footer.findViewById(R.id.spinner_notification_sound);
        tvKmPerHoursValue = (TextView) footer.findViewById(R.id.tv_km_per_hours_values);
        cbPlayNotification.setOnClickListener(new PlayerListener());
        etCustomNotification.setOnFocusChangeListener(new CustomNotificationFocusListener());
        etCustomNotification.setOnEditorActionListener(new KeyBoardListener());
        ivSubmitCustomNotification.setOnClickListener(new SubmitCustomNotificationListener());
        spinnerNotificationSound.setOnItemSelectedListener(new SpinnerNotificationSoundItemListener());
        ratio = PERCENT / (MAX_SEEK_BAR - MIN_SEEK_BAR);
        sbActivationSpeed.setMax(PERCENT);
        sbActivationSpeed.incrementProgressBy(ratio);
        sbActivationSpeed.setOnSeekBarChangeListener(new SeekBarActivationSpeedListener());
        rlCustom = (RelativeLayout) footer.findViewById(R.id.rl_custom);
        tvContactUs = (TextView) footer.findViewById(R.id.tv_contact_us);
        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Launcher.activity.contactUs(NotificationSettingsActivity.this);

            }
        });
        initFooterFonts(footer);
        lvRoot.addFooterView(footer);
    }

    private void initHeaderFonts() {
        FontsUtils.setGothic(getAssets(), rbTextNotification);
        FontsUtils.setGothic(getAssets(), rbVoiceNotification);
    }

    private void initFooterFonts(View footerView) {
        FontsUtils.setGothic(getAssets(), (TextView) footerView.findViewById(R.id.tv_set_reminder_notification_sound));
        FontsUtils.setGothic(getAssets(), (TextView) footerView.findViewById(R.id.tv_set_reminder_activation_speed));
        FontsUtils.setGothic(getAssets(), (TextView) footerView.findViewById(R.id.tv_km_per_hours));
        FontsUtils.setGothic(getAssets(), tvContactUs);
        FontsUtils.setGothic(getAssets(), tvKmPerHoursValue);
        FontsUtils.setGothic(getAssets(), etCustomNotification);
    }

    private void submitCustom() {
        final String customNotification = etCustomNotification.getText().toString();
        if (!customNotification.isEmpty()) {
            getSharedHelper().setCustomTextNotification(customNotification);
            getSharedHelper().setTextNotification(customNotification);
            textNotificationAdapter.addCustomTextNotification(customNotification);
            lvRoot.setItemChecked(textNotificationAdapter.positionByString(customNotification) + 1, true);
            etCustomNotification.setText("");
            SystemUtils.hideKeyboard(getApplicationContext(), etCustomNotification);
        } else {
            etCustomNotification.requestFocusFromTouch();
            SystemUtils.showKeyboard(getApplicationContext());
        }
    }

    private void stopPlayer() {
        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
        }
    }

    private int valueToPercent(int value) {
        return (value - MIN_SEEK_BAR) * ratio;
    }

    private int percentToValue(int percent) {
        return (percent / ratio) + MIN_SEEK_BAR;
    }

    private class SeekBarActivationSpeedListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                int speed = percentToValue(progress);
                tvKmPerHoursValue.setText(String.valueOf(speed));
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            int speed = percentToValue(seekBar.getProgress());
            getSharedHelper().setSpeed(Converter.inMetresPerSec(speed));
        }
    }

    private class PlayerListener implements View.OnClickListener, Player.OnSimplePlayerListener {

        PlayerListener() {
            player = new Player(this);
        }

        @Override
        public void onClick(View v) {
            if (!player.isInitInstance()) {
                int position = spinnerNotificationSound.getSelectedItemPosition();
                String path = soundNotificationAdapter.getItem(position).getPath();
                player.initInstance(getApplicationContext(), Uri.parse(path));
            }
            if (player.isPlaying()) {
                player.pause();
            } else {
                player.start();
            }
        }

        @Override
        public void onPlayerFinished() {
            cbPlayNotification.setChecked(false);
        }
    }

    private class SpinnerNotificationSoundItemListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            stopPlayer();
            getSharedHelper().setSoundPath(soundNotificationAdapter.getItem(position).getPath());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class ListViewNotificationTextListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            textNotificationPosition = position;
            stopPlayer();
            getSharedHelper().setTextNotification(textNotificationAdapter.getItem(position - 1));
        }
    }

    private class NotificationTypeListener implements RadioGroup.OnCheckedChangeListener {


        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            stopPlayer();
            switch (checkedId) {
                case R.id.rb_text_notification:
                    getSharedHelper().setTypeNotification(SharedHelper.TYPE_TEXT);
                    rlCustom.setVisibility(View.VISIBLE);
                    lvRoot.setAdapter(textNotificationAdapter);
                    lvRoot.setItemChecked(textNotificationPosition, true);
                    break;
                case R.id.rb_voice_notification:
                    getSharedHelper().setTypeNotification(SharedHelper.TYPE_VOICE);
                    rlCustom.setVisibility(View.GONE);
                    lvRoot.setAdapter(emptyAdapter);
                    break;
            }
        }
    }

    private class SubmitCustomNotificationListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            submitCustom();
        }
    }

    private class CustomNotificationFocusListener implements View.OnFocusChangeListener {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && !etCustomNotification.getText().toString().isEmpty()) {
                submitCustom();
            }
        }
    }

    private class KeyBoardListener implements TextView.OnEditorActionListener {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                submitCustom();
                return true;
            }
            return false;
        }
    }

}
